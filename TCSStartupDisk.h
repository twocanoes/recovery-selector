//
//  TCSStartupDisk.h
//  Recovery Selector
//
//  Created by Timothy Perfitt on 1/26/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSStartupDisk : NSObject <NSSecureCoding>
@property (strong) NSString *name;
@property (strong) NSString *mountpoint;
@property (strong) NSString *bootloader;
@property (assign) NSInteger type;
@property (assign) BOOL isCurrentStartupDisk;
@end

NS_ASSUME_NONNULL_END
