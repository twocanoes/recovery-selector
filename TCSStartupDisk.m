//
//  TCSStartupDisk.m
//  Recovery Selector
//
//  Created by Timothy Perfitt on 1/26/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSStartupDisk.h"

@implementation TCSStartupDisk

- (void)encodeWithCoder:(nonnull NSCoder *)encoder {

    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.mountpoint forKey:@"mountpoint"];
    [encoder encodeObject:self.bootloader forKey:@"bootloader"];
    [encoder encodeBool:self.isCurrentStartupDisk forKey:@"isCurrentStartupDisk"];
    [encoder encodeInteger:self.type forKey:@"type"];

}

- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    TCSStartupDisk *newStartupDisk=[[TCSStartupDisk alloc] init];

    newStartupDisk.name=[_name copy];
    newStartupDisk.mountpoint=[_mountpoint copy];
    newStartupDisk.bootloader=[_bootloader copy];
    newStartupDisk.isCurrentStartupDisk=_isCurrentStartupDisk;
    newStartupDisk.type=_type;


    return newStartupDisk;

}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.name=[decoder decodeObjectOfClass:[NSString class] forKey:@"name"];
    self.mountpoint=[decoder decodeObjectOfClass:[NSString class]  forKey:@"mountpoint"];
    self.bootloader=[decoder decodeObjectOfClass:[NSString class]  forKey:@"bootloader"];
    self.isCurrentStartupDisk=[decoder decodeBoolForKey:@"isCurrentStartupDisk"];
    self.type=[decoder decodeIntegerForKey:@"type"];

     return self;
}

+ (BOOL)supportsSecureCoding{

    return YES;

}

-(NSString *)description{

    return [NSString stringWithFormat:@"name: %@",self.name];

}


@end
