//
//  RecoverySelectorHelper.h
//  RecoverySelectorHelper
//
//  Created by Timothy Perfitt on 1/24/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#include <string.h>
#include <sys/stat.h>
#import <AppKit/AppKit.h>
#import "RecoverySelectorHelperProtocol.h"
#include <signal.h>

@interface RecoverySelectorHelper : NSObject <RecoverySelectorHelperProtocol,NSXPCListenerDelegate>
+ (NSString *)machServiceName;
+ (NSString *)exposedProtocolName;
- (void)runXPCService;

@end
