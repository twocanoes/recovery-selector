//
//  RecoverySelectorHelperProtocol.h
//  com.twocanoes.recoveryselectorhelper
//
//  Created by Timothy Perfitt on 1/24/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSStartupDisk.h"
// The protocol that this service will vend as its API. This header file will also need to be visible to the process hosting the service.
@protocol RecoverySelectorHelperProtocol
- (void)getVersionCallback:(void (^)(NSInteger))callback;

-(void)selectDisk:(TCSStartupDisk *)disk withCallback:(void (^)(NSInteger))callback;
-(void)unselectRecoveryWithCallback:(void (^)(NSInteger))callback;
-(void)startupDisksWithCallback:(void (^)(NSArray <TCSStartupDisk * > *))done;
@end

