//
//  TCSOvalButton.m
//  Winclone 6
//
//  Created by Tim Perfitt on 3/15/18.
//  Copyright © 2018 Twocanoes Software Inc. All rights reserved.
//

#import "TCSOvalButton.h"

@implementation TCSOvalButton


-(void)awakeFromNib{
    [self updateTitleColor];
    
}
-(void)setTitle:(NSString *)title{
    [super setTitle:title];
    [self updateTitleColor];
    
}
-(void)updateTitleColor{
    
    NSColor *color = [NSColor whiteColor];
    
    NSMutableAttributedString *colorTitle =
    [[NSMutableAttributedString alloc] initWithAttributedString:[self attributedTitle]];
    
    NSRange titleRange = NSMakeRange(0, [colorTitle length]);
    
    [colorTitle addAttribute:NSForegroundColorAttributeName
                       value:color
                       range:titleRange];
    
    [self setAttributedTitle:colorTitle];

}

@end
