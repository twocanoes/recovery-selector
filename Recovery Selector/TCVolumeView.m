//
//  TCVolumeView.m
//  Boot Manager
//
//  Created by Timothy Perfitt on 7/22/12.
//  Copyright (c) 2012 Twocanoes Software, Inc. All rights reserved.
//

#import "TCVolumeView.h"

@implementation TCVolumeView
//@synthesize delegate;
//-(void)mouseDown:(NSEvent *)theEvent{
//    [super mouseDown:theEvent];
//    if ([theEvent clickCount]>1) {
////        [self.delegate doubleClick:self];
////        if (self.delegate && [self.delegate respondsToSelector:@selector(doubleClick:)]) {
////            [self.delegate performSelector:@selector(doubleClick:) withObject:self];
////        }
//    }
//}

-(BOOL)acceptsFirstResponder{
    return NO;
}

- (void)drawRect:(NSRect)dirtyRect
{
    if (self.selected) {
       [[NSColor gridColor] set];
       NSRectFill([self bounds]);
    }
}


@end
