//
//  MDSPrivHelperToolController.h
//  MDS
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSStartupDisk.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSPrivHelperToolController : NSObject
- (void)installToolIfNecessary;
+ (instancetype)sharedHelper ;
- (void)getVersionCallback:(void (^)(NSInteger))callback;
-(void)selectDisk:(TCSStartupDisk *)diskInfo withCallback:(void (^)(NSInteger))callback;
-(void)unselectRecoveryWithCallback:(void (^)(NSInteger))callback;
-(void)startupDisksWithCallback:(void (^)(NSArray *))done;
@end

NS_ASSUME_NONNULL_END
