//
//  ViewController.m
//
//  Created by Timothy Perfitt on 1/24/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "ViewController.h"
#import "TCSPrivHelperToolController.h"
#include <stdio.h>
#include <CoreServices/CoreServices.h>
#include <Carbon/Carbon.h>
#import <sys/mount.h>
#import "TCSConstants.h"
#import "TCSStartupDisk.h"
#import <TCSPromo/TCSPromo.h>
#import "TCSOvalButton.h"
#import "TCSStartupDiskCollectionViewItem.h"
@interface ViewController()
@property (weak) IBOutlet NSCollectionView *diskCollectionView;
@property (strong) NSMutableArray *startupDisks;
@property (assign) BOOL isSelected;
@property (weak) IBOutlet TCSOvalButton *selectButton;
@end

@implementation ViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {

    }
    return self;
}

//- (void)encodeWithCoder:(nonnull NSCoder *)coder {
//    <#code#>
//}

-(BOOL)restartMac:(id)sender{
    OSStatus error = SendAppleEventToSystemProcess(kAERestart);
    if (error == noErr)
    {
        return YES;

    }
    else
    {
        return NO;
    }

}
- (IBAction)runCommandButtonPressed:(id)sender {

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Restart Mac?";
    alert.informativeText=@"Are sure you want to restart this Mac?";

    [alert addButtonWithTitle:@"Restart"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;
    [self restartMac:self];
}
- (void)collectionView:(NSCollectionView *)collectionView didSelectItemsAtIndexPaths:(NSSet<NSIndexPath *> *)indexPaths{

    [[TCSPrivHelperToolController sharedHelper] installToolIfNecessary];

    TCSStartupDisk *selectedDisk=[self.startupDisks objectAtIndex:[indexPaths.anyObject indexAtPosition:1]] ;
    [[TCSPrivHelperToolController sharedHelper] selectDisk:selectedDisk withCallback:^(NSInteger status) {


    }];




}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[TCSPrivHelperToolController sharedHelper] installToolIfNecessary];

    self.startupDisks = [NSMutableArray array];

    [[TCSPrivHelperToolController sharedHelper] startupDisksWithCallback:^(NSArray * disks) {

        [disks enumerateObjectsUsingBlock:^(TCSStartupDisk *disk, NSUInteger idx, BOOL * _Nonnull stop) {

            TCSStartupDisk *newDisk=[disk copy];

            NSString *volumeName=@"unknown";
            NSError *err;
            NSString *path=disk.mountpoint;
            if (path){
                NSURL *fileURL=[NSURL fileURLWithPath:path];
                [fileURL getResourceValue:&volumeName forKey:NSURLVolumeNameKey error:&err];
            }
            else {
                path=@"unknown";
            }
            NSString *bootloader=newDisk.bootloader;
            if (!bootloader) bootloader=@"unknown";
            newDisk.name=volumeName;
            newDisk.type=RECOVERYTYPEOS;
            [self.startupDisks addObject:newDisk];


            TCSStartupDisk *recoveryDisk=[disk copy];
            recoveryDisk.name=[volumeName stringByAppendingString:@" Recovery"];
            recoveryDisk.type=RECOVERYTYPERECOVERY;

            [self.startupDisks addObject:recoveryDisk];


            TCSStartupDisk *internetRecoveryDisk=[disk copy];
            internetRecoveryDisk.name=[volumeName stringByAppendingString:@" Internet Recovery"];
            internetRecoveryDisk.type=RECOVERYTYPEINTERNETRECOVERY;

            [self.startupDisks addObject:internetRecoveryDisk];





        }];
//        NSImage *image=[NSImage imageNamed:NSImageNameApplicationIcon];

        TCSStartupDisk *diagnosticDisk=[[TCSStartupDisk alloc] init];
        diagnosticDisk.name=@"Diagnostics";
        diagnosticDisk.mountpoint=@"";
        diagnosticDisk.bootloader=@"";
        diagnosticDisk.type=RECOVERYTYPEDIAGNOSTIC;

        [self.startupDisks addObject:diagnosticDisk];


        TCSStartupDisk *internetDiagnosticDisk=[[TCSStartupDisk alloc] init];
        internetDiagnosticDisk.name=@"Internet Diagnostics";
        internetDiagnosticDisk.mountpoint=@"";
        internetDiagnosticDisk.bootloader=@"";
        internetDiagnosticDisk.type=RECOVERYTYPEINTERNETDIAGNOSTIC;

        [self.startupDisks addObject:internetDiagnosticDisk];


        dispatch_async(dispatch_get_main_queue(), ^{
            [self.diskCollectionView reloadData];

        });
    }];

    NSNib *nib=[[NSNib alloc] initWithNibNamed:@"TCSStartupDiskCollectionViewItem" bundle:[NSBundle mainBundle]];
    [self.diskCollectionView registerNib:nib forItemWithIdentifier:@"diskItem"];
    [self updatedState:self];


    // Do any additional setup after loading the view.
}
-(void)updatedState:(id)sender{
    self.isSelected=[self isRecoverySelected:self];

//    if (self.isSelected==YES) {
//
//        self.selectButton.title=@"Unselect Recovery Partition";
//
//    }
//    else{
//
//        self.selectButton.title=@"Select Recovery Partition";
//    }

}

-(BOOL)isRecoverySelected:(id)sender{
    NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/usr/sbin/nvram" arguments:@[@"recovery-boot-mode"]];

    [task waitUntilExit];
    if (task.terminationStatus==0) {

        return YES;

    }
    return NO;
}
- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}
OSStatus SendAppleEventToSystemProcess(AEEventID EventToSend)
{
    AEAddressDesc targetDesc;
    static const ProcessSerialNumber kPSNOfSystemProcess = { 0, kSystemProcess };
    AppleEvent eventReply = {typeNull, NULL};
    AppleEvent appleEventToSend = {typeNull, NULL};

    OSStatus error = noErr;

    error = AECreateDesc(typeProcessSerialNumber, &kPSNOfSystemProcess,
                         sizeof(kPSNOfSystemProcess), &targetDesc);

    if (error != noErr)
    {
        return(error);
    }

    error = AECreateAppleEvent(kCoreEventClass, EventToSend, &targetDesc,
                               kAutoGenerateReturnID, kAnyTransactionID, &appleEventToSend);

    AEDisposeDesc(&targetDesc);
    if (error != noErr)
    {
        return(error);
    }

    error = AESend(&appleEventToSend, &eventReply, kAENoReply,
                   kAENormalPriority, kAEDefaultTimeout, NULL, NULL);

    AEDisposeDesc(&appleEventToSend);
    if (error != noErr)
    {
        return(error);
    }

    AEDisposeDesc(&eventReply);

    return(error);
}


- (nonnull NSCollectionViewItem *)collectionView:(nonnull NSCollectionView *)collectionView itemForRepresentedObjectAtIndexPath:(nonnull NSIndexPath *)indexPath {

    TCSStartupDiskCollectionViewItem *collectionViemItem=[collectionView makeItemWithIdentifier:@"diskItem" forIndexPath:indexPath];
    TCSStartupDisk *currDisk=[self.startupDisks objectAtIndex:[indexPath indexAtPosition:1]];
    NSImage *image;
    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:currDisk.mountpoint]==YES){
       image= [[NSWorkspace sharedWorkspace] iconForFile:currDisk.mountpoint];
    }
    else
    {
        image=[NSImage imageNamed:NSImageNameApplicationIcon];
    }


    collectionViemItem.imageView.image=image;
    if (currDisk.name) collectionViemItem.textField.stringValue=currDisk.name;
    
        return collectionViemItem;
}

- (NSInteger)collectionView:(nonnull NSCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return [self.startupDisks count];
}

//- (BOOL)commitEditingAndReturnError:(NSError *__autoreleasing  _Nullable * _Nullable)error {
//
//}

@end
