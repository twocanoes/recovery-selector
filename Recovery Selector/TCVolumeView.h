//
//  TCVolumeView.h
//  Boot Manager
//
//  Created by Timothy Perfitt on 7/22/12.
//  Copyright (c) 2012 Twocanoes Software, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@interface TCVolumeView : NSView{
    
    IBOutlet NSTextView *descriptionTextView;
}

@property (weak) NSCollectionView *delegate;
@property (readwrite) BOOL selected;

@end
