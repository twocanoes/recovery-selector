//
//  AppDelegate.m
//
//  Created by Timothy Perfitt on 1/24/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "AppDelegate.h"
#import <TCSPromo/TCSPromoManager.h>

@interface AppDelegate ()

@end

@implementation AppDelegate
- (IBAction)showPromotions:(id)sender {
    [self showPromos:self];
}
-(void)showPromos:(id)sender{
    [[TCSPromoManager sharedPromoManager] showPromoWindow:self];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender{
    return YES;
}
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {


    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"hidePromo"] boolValue]==NO) {
        [[TCSPromoManager sharedPromoManager] showPromoWindow:self];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"];
    }


}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
