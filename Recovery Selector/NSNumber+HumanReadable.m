//
//  NSNumber+HumanReadable.m
//  DiskDevice
//
//  Created by Steve Brokaw on 11/21/17.
//  Copyright © 2017 Twocanoes, Inc. All rights reserved.
//

#import "NSNumber+HumanReadable.h"

@implementation NSNumber (HumanReadable)

- (NSString *)stringWithHumanReadableInt {

    double value = self.doubleValue;

    NSString *transformedVal = (NSInteger)(value/pow(10,9))>0?[NSString localizedStringWithFormat:@"%2.2f GB",value/pow(10,9)]:
    (NSInteger)(value/pow(10,6)) > 0 ? [NSString localizedStringWithFormat:@"%2.2f MB",value/pow(10,6)]:
        (NSInteger)(value/pow(10,3)) > 0 ? [NSString localizedStringWithFormat:@"%2.2f KB",value/pow(10,3)] : value==0 ? @"" : [NSString localizedStringWithFormat:@"%2.2f Bytes",value];
    return transformedVal;
}

@end
