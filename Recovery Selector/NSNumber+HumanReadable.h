//
//  NSNumber+HumanReadable.h
//  DiskDevice
//
//  Created by Steve Brokaw on 11/21/17.
//  Copyright © 2017 Twocanoes, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (HumanReadable)

-(NSString *)stringWithHumanReadableInt;

@end
